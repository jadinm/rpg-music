Battle
======

Battle
------

Goblin Slayer - Subjugation Strategy - https://www.youtube.com/watch?v=gUYcs1sj3Q4

Konosuba - Taiji (Megumi) - https://www.youtube.com/watch?v=a9P1dKf6iVA

Goblin Slayer - Beating - https://www.youtube.com/watch?v=qT4iZZ1Mmdg

Boss Battle - Hope
------------------

Sword Art Online - We Have To Defeat It - https://www.youtube.com/watch?v=Mp6uzqMNTeU

Sword Art Online - False King - https://www.youtube.com/watch?v=LZNSqPLhVtc

Sword Art Online - The Destined Battle - https://www.youtube.com/watch?v=aEzCmLgMiDE

Sword Art Online - A Desperate Battle, No Way Back - https://www.youtube.com/watch?v=7qxq02xIVos

RE:Zero - Start of Fate - https://www.youtube.com/watch?v=SKuX-hsJdg4

Shield Hero - Mirrors - https://www.youtube.com/watch?v=SILJdcOKuUU

Boss Battle - Despair
---------------------

RE:Zero - Hymne of Despair and Atonement - https://www.youtube.com/watch?v=QBHk1ld9tQk

Log Horizon - Monster Shuurai - https://www.youtube.com/watch?v=EI6OfY1h1Nc&list=PLMy-e1JL4mgvDHXin6IsfnAu4zpzqbpwU&index=26

Overlord - Power of Nazarick - https://www.youtube.com/watch?v=QDQai8lO2EU

Sword Art Online - Sword Golem - https://www.youtube.com/watch?v=9gbw4WSZGFo

Shield Hero - WAVES - https://www.youtube.com/watch?v=oS0ImlKNgbw

