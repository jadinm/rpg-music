Tension music
=============

Creepy
------

Mirai Nikki - Yuno's Main Theme - https://www.youtube.com/watch?v=IwNlmpy_a2E
Shiki - Silent Night - https://www.youtube.com/watch?v=z-JXV7tHHRw
Log Horizon - Fuon Na Kage - https://www.youtube.com/watch?v=9yP-oyzcJaM&list=PLMy-e1JL4mgvDHXin6IsfnAu4zpzqbpwU&index=8

Tension latente
---------------

Shield Hero - Parliament - https://www.youtube.com/watch?v=MJgTMaeQxqM
Overlord - Dōka nasaimashita ka, momonga-sama - https://www.youtube.com/watch?v=BosCPSQVW_k

High tension
------------

Shield Hero - Beloukas - https://www.youtube.com/watch?v=YyS2Dq0356Q

Infiltration / Antre de Boss
----------------------------

Overlord - Strategy Planning - https://www.youtube.com/watch?v=kfJfhST5nbU

Poursuite
---------

Shield Hero - Autoroll - https://www.youtube.com/watch?v=36qIE4P92hY

